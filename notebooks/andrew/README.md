# 1/21: first meeting, online 
    Talked about our ideas/goals with the program, and started working on what would become our RFA. added a post to Pace to get confirmation from TA/Professors
# 1/23: in person RFA completion
    Did what it says on the tin.
    Worked on finishing the RFA, and a general plan for how exactly the whole system's interfaces were going to work.
    Basic design splits into a PC software side, where the STT, TTS, and ChatGPT interactions will happen,
    and a Board side that contains a screen, audio I/O, and a microcontroller to manage the ins and outs of those components. 
![image info](IMG_9008.JPG)

# 2/1: 2nd meeting
    Started Project proposal, although mostly just worked on picking components for PCB (microcontroller, audio parts, etc.)
    Wrote up Ethical concern section, tolerance analysis.
    
# 2/6: Project proposal
    worked on project proposal as a group, finished and submitted. 
    Specifically found all relevant power consuming parts and did power budget,
    Collaborated on requirements
# 2/17: Started Design Doc
    Added initial parts list, cost estimates, and physical design section
# 2/21: Finish proposal revision
    For Proposal, rewrote the requirements section as equations, 
    Added IEEE code to the Ethics section
    For Design Doc, Formatted and edited, also wrote and agreed upon timeline 

# 2/28: finished first PCB design
    Still need to work on the board layout, but we can start to order parts now
    See zipped files
# 3/3: Design review
    Prof says we need more hardware elements, 
    will be adding amps to both speaker and microphone on pcb to make it work well
    -must revise PCB design to accomodate these new devices
    
# 3/19-21: Post break work:
    Ordered the components for everything but the amp/sound thing we need to add
    Still need to finish pcb revision and order
    found good TI documentation on design for PCBs for power, will reference: https://www.ti.com/lit/an/sboa237/sboa237.pdf
    I'm a little confused on how to integrate and work all this, 
    I'll reach out if I have more questions

# 3/21-22:  Sch and footprint finding
    Finding random footprints for obscure items is a pain, 
    particularly the potentiometers and pins for the screen. 
    Often a footprint would be available, but not a sellable part, or vice versa.
    had to add footprints for all the new items added for the audio equipment,
    now reflected in the new PCB.
# 3/23-24: PCB Design and ordering
    Stuck everything onto the board, trying to keep the audio content separate and as far away from other noisy channels as possible, doing everything but the silkscreening on the 23
    Put silk screening on for all parts as well as our team and revision number, then placed an order with JLCPCB, should arrive this coming week (27-31)
# 3/31: First PCB soldering
    First parts order arrived, and started soldering. 
    Ran into some challenges with soldering onto the tiny tiny pins, 
    and realized we had ordered a few slightly incorrect components.
    Did what we could, and ordered new parts for the connector to the screen, audio parts, and the USB type B connector (data to and from microprocessor). 
    Couple of issues with the board discovered: 
    first, the pin pitch for the screen is way larger than the one on the board, this is primarily my fault. 
    Molex makes connectors that will work, so this is more of a cost issue than anything else. 
    Second, a couple of the ICs we planned on using are not having their Vcc powered by the board. 
    I think this comes down to how I was doing the packaging on KiCAD (I'm more familiar with EAGLE), but it's also fixable with some jumper wires. 
![image info](IMG_9461.JPG)
# 4/10: Second PCB Soldering day
    Parts arrived over the weekend, but I was very ill. 
    Fully assembled the microprocessor system so that others could start to work on it, but still having some issues with the screen/audio components.
    Another ordering issue, we ordered the wrong size of 1692 IC. we ordered a much smaller one than the standard size, and so it does not fit on the board. 
    Luckily I had some other OP-Amps with similar properties and the correct form factor laying around, so it wasn't a big deal but that likely will affect overall sound quality as the noise properties were much worse on the one I am using.
    In order to expedite the PCB process, I ordered the PCB using 0805 form factors for all RC components so we could order it then figure out the value for the audio components afterwards. 
    While this worked perfectly for the Microprocessor, some of the planned audio devices have Capacitance values way too high for 0805 package sizes, like 1000 uf. 
    There's a few ways to circumvent this but none are particularly professional or good looking. Current strategy is going to be to use through hole capacitors and just solder them to the contacts. 
    Not pretty, but certainly effective. Other option is to stack 0805 caps on top of each other (since parallel caps add, you can just stack them on top of each other to achieve high values), but this seems even less professional and even harder. 

    passed off the board so we can start the networking/communication parts,
    then I can finish up the soldering when we have the kinks ironed out

# 4/13: Final soldering day
    Finished up all the soldering on the whole board, so it's ready for total system integration. 
    This involved a few interesting solders, including a large 1000 uf through-hole capacitor being bolted to a 0805 solder join, 
    as well as two series 100 nf capacitors to get the desired ~50 uf system. 
    overall quite successful,  
![image info](IMG_9455.JPG)
# 4/16: Glue code day
    Worked on connecting all the different individual systems that Brahmteg had made (TTS, STT, OpenAI, etc.).
     All worked through terminal, but the system works well enough.
    Added a ding sound effect when it started listening so that users wouldn't talk before it was listening. 

    Also worked on sorting out code vs actual text from the chatGPT response.
    They put triple quotes around code (but also around certain bits inside of the code, so it's less trivial of a problem than you might think).
    Have it working now, where the TTS only says non-code, and we can send that code to a different program to have it be whipped into a code framework. 

# 4/23: Crunch Time
    Tried unsuccessfully to migrate system to an arduino Mega, so it's going to be a fully software project
    Worked on the Tkinter GUI and testcases so we have something to present


# 4/27: Presentation work
    Made Presentation
    Wrote up conclusion/further work sections,
    Edited down intro sections
# 4/28: Presentation work/Mock Pres
    Mostly just editing work today, had the mock presentation.
    Mock went well, just need to eliminate filler content a bit from my speech

# 4/30: Presentation work
    Finished presentation, added speaker notes indicating which slides were whose.
    Added sections on PCB Design, as well as reviewing the original design
