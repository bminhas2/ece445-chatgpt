# 3/03: Text-To-Speech Demo
Created a text-to-speech demo using python's Google Text-To-Speech package. 

This can be found in
```\code\TTS\Text-To-Speech.py```

Followed the instructions [here][TTS_Link] as a guide. 

There were a few things that I had to do to get this demo running besides the tutorial. 

Firstly, my python download (on MacOS) was not setup to use TK. I fixed this by running the commmand 

    brew install python-tk

I also had to instill the gtts and playsound libraries using:

    python3 -m pip install <library>

Finally, I had to install the PyObjC library to allow python to update my audio to play through the built in speakers. 

After these steps, the demo worked correctly. It displays a GUI which contains a box for the user to input text. After pressing ```play```, it stores the audio translation to an .mp3 file and plays the file through audio out. 

Once the project is further along, I will streamline this demo to perform TTS on a file instead of user-input text and only output an mp3 file and not the speaker audio. 

[TTS_Link]: https://data-flair.training/blogs/python-text-to-speech/#:~:text=gTTS%20

# 3/09: Beginning Speech To Text

Because of our speech-to-text use case, we need to retrain an existing model to recognize **"Hey Aidan"** as a wake word in a variety of deliveries. As such, we need a lot of audio files of people saying the wake word. I asked some of my friends to send audio files and will source more samples. 

Until then, I am setting up the base model and testing it to see if it generally works. 

# 3/21 Working on PCB Changes 

Our PCB needs to be finalized and ordered so I am doing some research into how to introduce a volume control and gain into the speaker. There are multiple op-amp choices that we can use. The most important thing is to reduce noise as much as possible but op-amps operating at 5V aren't very good at that. Because of this, we are considering using the TL072: an op-amp that is well reviewed and recommended for HiFi audio. The only issue is that according to the TL072 [documentation][TL_datasheet], we need 7V+ to power the op-amp. The alternative is the LM386 which operates at 5V but is considerable noisier according to the tests done [here](https://therepaircafe.wordpress.com/2021/03/20/in-defense-of-the-lm358/)

As a team, we're discussing whether the tradeoff between upgrading our power subsystem vs. going for a noisier op-amp and keeping the system at 5V

[TL_datasheet]: https://www.ti.com/product/TL072#tech-docs

# 3/22 Finalizing PCB Changes 

The OPA1692 fits our needs perfectly as an op-amp. It has a balanced amount of noise for its 3V+ voltage requirement ([OP1692_Datasheet][OP_datasheet]). It is recommended for audio projects and should keep noise well below the amount it is noticeable at. 

I made a schematic for a volume knob and gain circuit for the speaker in KiCad using recommended potentiometer values and [this project](https://www.electronicshub.org/lm386-audio-amplifier-circuit/) as a referance, and sent it to my group to integrate into the final schematic. 

Here is the schematic: 

![Speaker Schematic](Speaker_Schematic.png)

[OP_datasheet]: https://www.ti.com/lit/ds/symlink/opa1692.pdf?ts=1679531117137&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FOPA1692



# 3/27 Building Initial Speech-To-Text

Researching existing speech-to-text models that exist, we need one that we can augment with our own data of people saying "Hey Aidan" to ensure that "Hey Aidan" is always recognized when spoken.

DeepSpeech seems to fit the bill. I am testing using DeepSpeech by following [this](https://www.assemblyai.com/blog/deepspeech-for-dummies-a-tutorial-and-overview-part-1/) website.

The first problem I ran into was with the ```pip install webrtcvad pyqt5``` command. 

The install of webrtcvad required Microsoft Visual C++ 14.0 which I didn't have on my computer. I downloaded it from [here](https://visualstudio.microsoft.com/visual-cpp-build-tools/) by selected the "Desktop Development with C++" package. 

The DeepSpeech tutorial did not contain the imports for the libraries needed to run the model. I added these libraries: 

```
import wave
import collections
import webrtcvad
from deepspeech import Model
import timer
import glob
import os
import numpy as np
```

After adding the pretrained models to the ```./models``` folder as instructed, the ```main()``` function still had difficulty parsing the model. I left off working on debugging this error.

# 4/05 Finishing Speech To Text

I decided to switch from DeepSpeech to OpenAI's whisper speech to text library. The reason was that whisper seemed to be extremely accurate, open source and free to use as well as much simpler to implement. 

I followed the instructions on the [Whisper GitHub Repoository](https://github.com/openai/whisper)
to install whisper locally. 

I had to manually downgrade my python version to 3.10 as whisper cannot be installed on python 3.11.2

I added an audio file (```audio.mp3```) with the words "Hey Aidan" and running a python script to trascribe that file produced the correct output. 

Since this whole project uses python on the PC side, I am now looking into creating a python virtual environment for the project so that it is easy to setup on a new PC without installing all the dependencies individually. 

# 4/06 Using ChatGPT API