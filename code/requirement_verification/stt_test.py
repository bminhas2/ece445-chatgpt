import os
import speech_recognition as sr

def stt_test() -> list:
    speech_texts = []
    r = sr.Recognizer()
    for filename in os.listdir(dir):
       f = os.path.join(dir, filename)
       speech_text = r.recognize_google(f)
       speech_texts.append(speech_text)
    return speech_texts
       
       