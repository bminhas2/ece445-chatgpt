import openai
import time

def chatgpt_api() -> list:
    openai.api_key = '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'
    timing_list = []
    start = time.time()
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt="how do I reverse a linked list in python",
        temperature=0.5,
        max_tokens=256,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0
    )
    print(response)

    end = time.time()
    timing_list.append(end - start)

    start = time.time()
    response = openai.Completion.create(
        engine="text-ada-001",
        prompt="why is openai the best company in the world",
        temperature=0.5,
        max_tokens=256,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0
    )
    end = time.time()
    timing_list.append(end - start)
    return timing_list

