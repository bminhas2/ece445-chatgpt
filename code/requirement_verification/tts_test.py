from gtts import gTTS
import os
from playsound import playsound

def tts(ttsMessageList):
    ttsMessage=""
    for i in range(0, len(ttsMessageList), 2):
        print(ttsMessageList[i])
        ttsMessage += ttsMessageList[i].replace("`","")
    speech = gTTS(text = ttsMessage)
    filename = 'DataFlair.mp3'
    if os.path.exists(filename):
        os.remove(filename)
    speech.save(filename)
    playsound(filename)