

def check_for_wake_word(input_strings) -> list:
    wake_word_in_string = []
    for string in input_strings:
        if "Aiden" in string or "Aidan" in string:
            wake_word_in_string.append(True)
        else:
            wake_word_in_string.append(False)
    return wake_word_in_string