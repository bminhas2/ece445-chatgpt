import unittest
from chat_gpt_test import chatgpt_api
from wake_word_test import check_for_wake_word
from stt_test import stt_test

class chatgptapi(unittest.TestCase):
    def test_api_call(self):
        result = chatgpt_api()
        print(result)
        message = "chatgpt api returns in greater than 4 seconds"
        
        for time in result:
            self.assertLess(time, 4, message)

class wake_word(unittest.TestCase):
    #later add audio files with each of these phrases
    def test_wake_word_detection(self):
        input_strings = ["Hey Aidan", "Hey Aiden", "Hi Aiden", "Can you aid in"]
        contains_wake_word = check_for_wake_word(input_strings=input_strings)
        self.assertEqual(contains_wake_word, [True, True, True, False], "Incorrect handling of wake word")

class stt_test(unittest.TestCase):
    def test_stt(self):
        output_text = stt_test()
        correct_output_test =[]
        self.assertEqual(len(output_text), len(correct_output_test), "Error with test function")
        for i in range(len(correct_output_test)):
            self.assertEqual(output_text[i], correct_output_test[i], "Incorrect classification by STT")
        
        
if __name__ == '__main__':
    unittest.main()