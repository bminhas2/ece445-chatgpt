import tkinter as tk
from gtts import gTTS
import os
from playsound import playsound
import speech_recognition as sr
import openai
import time

def test_tts(input_string):
    speech = gTTS(text = input_string)
    filename = 'test_tts.mp3'
    if os.path.exists(filename):
        os.remove(filename)
    speech.save(filename)
    playsound(filename)
    
def test_stt():
    r = sr.Recognizer()
    r.pause_threshold = 2
    
    mic = sr.Microphone()

    # Set the recognizer energy threshold
    with mic as source:
        r.adjust_for_ambient_noise(source)

    # Listen for the keyword
    playsound('Ping.wav')
    while True:
        with mic as source:
            audio = r.listen(source)
            speech_text = r.recognize_google(audio)
            return speech_text
        
def test_wake_word():
    keywords = ["Hayden","Haydn","Aiden","AI Dan"]
    r = sr.Recognizer()
    r.pause_threshold = 2
    
    mic = sr.Microphone()

    # Set the recognizer energy threshold
    with mic as source:
        r.adjust_for_ambient_noise(source)

    # Listen for the keyword
    playsound('Ping.wav')
    while True:
        with mic as source:
            audio = r.listen(source)
            speech_text = r.recognize_google(audio)
            for keyword in keywords:
                if keyword.lower() in speech_text.lower():
                    return True
                else: 
                    return False

def test_speed():
    requests = ["Can you tell me a joke that always makes people laugh?","What's the most interesting fact you know about history?",
     "How can I improve my writing skills?",
     "What's the best way to stay motivated when working on a long-term project?",
     "Can you recommend a good book for me to read?",
     "What's the best way to approach a difficult conversation with a friend?",
     "Can you give me some tips for staying organized and productive?",
     "What's the most important thing to consider when starting a new business?",
     "Can you explain a complex concept in a simple way?",
     "What's your opinion on the impact of technology on society?"]
    time_list = []
    openai.api_key = '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'
    for request in requests:
        start = time.time()
        response = openai.Completion.create(
            engine="text-davinci-002",
            prompt=request,
            temperature=0.5,
            max_tokens=256,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0
        )
        end = time.time()
        time_list.append(end-start)
    avg_time = sum(time_list)/len(time_list)
    if avg_time <= 4:
        return f"Speed test passed with an average time of {avg_time}"
    else: 
        return f"Speed test failed with an average time of {avg_time}"

    
    