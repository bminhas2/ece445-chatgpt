from Listener import transcribe_speech_after_keyword as transcribe
from gtts import gTTS
import os
from playsound import playsound
import openai
import tkinter as tk
from tkinter import TOP, BOTH
from pygments import highlight
from pygments.lexers import guess_lexer
from PIL import Image, ImageTk
from pygments.formatters.img import ImageFormatter
from threading import Thread
from copy import deepcopy
import librosa

#code flow:

## 
# 1: import modules and setup streams/connections 
# 2: setup the main loop. Listen for the wakeword.
# 3: if wakeword is detected, send next audio clip to the ChatGPT API
# 4: if the ChatGPT API returns any non-markdown content, send that content to the TTS API (```, 3 backticks)
# 5: if the ChatGPT API contains markdown, send the markdown to a display module
# 6: go to step 2.
##

##
#interfaces
#
#
##    

def output_audio(ttsMessageList) -> None:
    ttsMessage=""
    for i in range(0, len(ttsMessageList), 2):
        print(ttsMessageList[i])
        ttsMessage += ttsMessageList[i].replace("`","")
    speech = gTTS(text = ttsMessage)
    filename = 'DataFlair.mp3'
    if os.path.exists(filename):
        os.remove(filename)
    speech.save(filename)
    playsound(filename, block=False)

    

class ScrollableFrame(tk.Frame):
    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)

        # create a canvas widget and add a scrollbar to it
        self.canvas = tk.Canvas(self, bg='white')
        self.scrollbar = tk.Scrollbar(self, orient='vertical', command=self.canvas.yview)
        self.canvas.config(yscrollcommand=self.scrollbar.set)

        # add a frame to the canvas to hold the content
        self.inner_frame = tk.Frame(self.canvas, bg='white')
        self.inner_frame.bind('<Configure>', self._on_frame_configure)

        # add the canvas and scrollbar to the main frame
        self.canvas.pack(side='left', fill=BOTH, expand=True)
        self.scrollbar.pack(side='right', fill='y')
        self.canvas.create_window((0, 0), window=self.inner_frame, anchor='nw')

    def _on_frame_configure(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

class TextImageDisplay(tk.Frame):
    def __init__(self, parent, data_list):
        super().__init__(parent)

        # create a ScrollableFrame to hold the content
        self.scrollable_frame = ScrollableFrame(self)
        self.scrollable_frame.pack(side=TOP, fill=BOTH, expand=True)

        # add the content to the ScrollableFrame
        for data in data_list:
            if data['type'] == 'text':
                # create a label to display text
                text_label = tk.Label(self.scrollable_frame.inner_frame, text=data['content'], wraplength=800, justify='left', font=('Arial', 24), fg = 'black', bg='white')
                text_label.pack(side=TOP, fill=BOTH, padx=10, pady=4)
            elif data['type'] == 'image':
                # load the image from file and create a label to display it
                image = Image.open(data['content'])
                photo = ImageTk.PhotoImage(image)
                image_label = tk.Label(self.scrollable_frame.inner_frame, image=photo, bg='white')
                image_label.image = photo
                image_label.pack(side=TOP, fill=BOTH, padx=10, pady=10)


def mainloop():
    data_list = []
    window = tk.Toplevel()
    while True:
        for filename in os.listdir('images/'):
            os.remove('images/' + filename)
        Message = []
        message_thread = Thread(target=transcribe, args=(("Hayden","Haydn","Aiden","AI Dan"), Message,))
        message_thread.start()
        message_thread.join()

        #Message = transcribe(("Hayden","Haydn","Aiden","AI Dan"))    
        openai.api_key = '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'

        #chatGPT can be busy, so we need to make sure to include a try/except block for this
        completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
        {"role": "user", "content": Message[0]}
        ]
        )
        
        sections = []
        GPTMessage = completion.choices[0].message.content
        ttsMessage = GPTMessage
        with open('output.txt', 'w') as file:
            # Write the string to the file
            file.write(GPTMessage)
        sections += GPTMessage.split('```')
        while "```" in ttsMessage:
            first_index = ttsMessage.find("```")
    # Find the index of the second occurrence, starting from the index of the first occurrence
            second_index = ttsMessage.find("```", first_index + 3)
    # Remove the text between the two occurrences
            if(second_index == -1): #if there is only one, dont get rid of it
                break
            ttsMessage = ttsMessage[:first_index] + ttsMessage[second_index + len("```"):]
            
        # loop through each section and highlight the code sections
        sections_copy = deepcopy(sections)
        thread = Thread(target = output_audio, args=(sections_copy,))
        thread.start()
        for i in range(len(sections)):
            if i % 2 == 0:
                # this is a non-code section, so add it to the highlighted sections list
                data_list.append({'type': 'text', 'content': sections[i]})
            else:
                # this is a code section, so highlight it with pygments and add it to the highlighted sections list
                lexer = guess_lexer(sections[i])
                open('images/short' +str(i) + '.png', 'wb').write(highlight(sections[i], lexer, ImageFormatter()))
                data_list.append({'type': 'image', 'content': 'images/short' +str(i) + '.png'})
        display = TextImageDisplay(window, data_list)
        display.pack(fill=BOTH, expand=True)
        window.geometry('700x350')
        while thread.is_alive():
            continue
        file_length = float(librosa.get_duration(filename = 'DataFlair.mp3'))*1000
        window.after(ms = int(file_length), func = mainloop)
        window.mainloop()

