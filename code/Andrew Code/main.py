from MainLoop import mainloop
import tkinter as tk 
from tkinter import ttk
from threading import Thread
import testsuite
from testsuite import test_tts, test_stt, test_wake_word, test_speed
import socket 
import time 


def find_tcp_port(port):
    num_failures = 0
    while True:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(1)
            sock.connect(('localhost', port))
            sock.close()
            return True
        except:
            num_failures += 1
            if num_failures == 6:
                print("Number of Failures exceeded 5. Will keep trying silently")
            elif num_failures <= 5:
                print(f"Failed to connect to port {port}. Num failures: {num_failures}")
            time.sleep(5)

def main():
    ESP_32_PORT = 65432
    thread = Thread(target=find_tcp_port, args=(ESP_32_PORT,))
    thread.start()
    root = tk.Tk()
    mainloop_button = ttk.Button(root, text = "Run MainLoop", command = mainloop)
    mainloop_button.pack(side = 'top')
    
    tts_textbox = ttk.Entry(root)
    tts_textbox.pack(side = 'top')

    def tts_test():
        tts_text = tts_textbox.get()
        test_tts(tts_text)
        return
    
    test_tts_button = ttk.Button(root, text = "Test TTS", command = tts_test)
    test_tts_button.pack(side = 'top')
    
    stt_frame = tk.Text(root, height = 5, width = 50)
    stt_frame.pack(side = 'top')

    def stt_test():
        text_out = test_stt()
        stt_frame.insert(tk.END, text_out)
        return
    test_stt_button = ttk.Button(root, text = "Test STT",command = stt_test)
    test_stt_button.pack(side = 'top')

    
    wake_word_frame = tk.Text(root, height = 5, width = 50)
    wake_word_frame.pack(side = 'top')   

    def wake_word_test():
        was_found = test_wake_word()
        if was_found:
            wake_word_frame.insert(tk.END, "Wake Word Found")
        else:
            wake_word_frame.insert(tk.END, "Wake Word Not Found")
        
        return

    test_wake_word_button = ttk.Button(root, text = "Test Wake Word", command = wake_word_test)
    test_wake_word_button.pack(side = 'top')
 
    speed_frame = tk.Text(root, height = 1, width = 50)
    speed_frame.pack(side = 'top')
    
    def speed_test():
        speed = test_speed() 
        speed_frame.insert(tk.END, speed)
        return

    test_speed_button = ttk.Button(root, text = "Test Speed", command = speed_test)
    test_speed_button.pack(side = 'top')

    #testSuite_button = ttk.Button(root, text = "Run Test Suite", command = None)
    #testSuite_button.pack(side = 'top')
    root.mainloop()
    
if __name__ == '__main__':
    main()