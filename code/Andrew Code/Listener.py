import speech_recognition as sr
from playsound import playsound




def remove_text_before_word(string, target_word):
    words = string.split()
    try:
        index = words.index(target_word)
        return ' '.join(words[index+1:])
    except ValueError:
        return string
    

def transcribe_speech_after_keyword(keywords, output_list):
    # Initialize the recognizer and microphone
    r = sr.Recognizer()
    r.pause_threshold = 2
    
    mic = sr.Microphone()

    # Set the recognizer energy threshold
    with mic as source:
        r.adjust_for_ambient_noise(source)

    # Listen for the keyword
    playsound('Ping.wav')
    while True:
        with mic as source:
            audio = r.listen(source)
        try:
            # Recognize the keyword
            speech_text = r.recognize_google(audio)
            print(speech_text)
            for keyword in keywords:
                if keyword.lower() in speech_text.lower():
                    output_list.append(remove_text_before_word(speech_text, keyword))
                    return output_list
        except sr.UnknownValueError:
            print("Could not understand audio")
