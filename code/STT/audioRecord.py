import pyaudio
import wave
import subprocess
import os

# Set the audio settings
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
CHUNK = 1024
INTERVAL_SECONDS = 5
OVERLAP_SECONDS = 1
RECORD_SECONDS = INTERVAL_SECONDS + (OVERLAP_SECONDS * 4)
MP3_OUTPUT_DIR = "output"
if not os.path.exists(MP3_OUTPUT_DIR):
    os.makedirs(MP3_OUTPUT_DIR)

# Initialize PyAudio
audio = pyaudio.PyAudio()

# Record the audio
for i in range(5):
    # Set the output filenames
    WAVE_OUTPUT_FILENAME = f"output_{i}.wav"
    MP3_OUTPUT_FILENAME = f"{MP3_OUTPUT_DIR}/output_{i}.mp3"

    # Open the microphone stream
    stream = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True,
                        frames_per_buffer=CHUNK)

    # Record the audio
    frames = []
    for j in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

        # If we've recorded a full interval, save it as a WAV file
        if j > 0 and j % (RATE * INTERVAL_SECONDS) == 0:
            waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
            waveFile.setnchannels(CHANNELS)
            waveFile.setsampwidth(audio.get_sample_size(FORMAT))
            waveFile.setframerate(RATE)
            waveFile.writeframes(b''.join(frames))
            waveFile.close()

            # Convert the WAV file to MP3 format using FFmpeg
            subprocess.call(['ffmpeg', '-i', WAVE_OUTPUT_FILENAME, MP3_OUTPUT_FILENAME])

            # Delete the WAV file
            subprocess.call(['rm', WAVE_OUTPUT_FILENAME])

        # If we've passed the overlap period for this interval, remove the first set of frames
        if j > 0 and j % (RATE * OVERLAP_SECONDS) == 0:
            frames = frames[CHUNK:]

    # Stop the microphone stream
    stream.stop_stream()
    stream.close()

audio.terminate()
